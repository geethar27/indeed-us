﻿// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
// ********************* For testing local URL
//"chrome_url_overrides": {"newtab": "http://localhost/TNTier.UI.Web/admin/SearchTextDetails.aspx" },
// "update_url": "http://localhost/TNTier.Hosts.Web/" 
//"http://localhost:2974/"

// ********************* For Live URL
//"chrome_url_overrides": {"newtab": "https://www.tender.net.au/admin/SearchTextDetails.aspx" },
// "update_url": "https://tender.net.au/TNTier.Host/" 
// "new url": "http://103.213.192.114:3307/TNTier.Host/" 

//Global variable
var questions = [];
var isopenclicked = false;
var isopenAllclicked = false;
var getText1 = [];
var tabSampleId = 0;
var franchiseId = "5";
//var cookies = ['www.indeed.co.uk', 'www.indeed.co.uk'];
//var jobSiteUrl = "www.indeed.co.uk";
var cookies = ['www.indeed.com', 'www.indeed.com'];
var jobSiteUrl = "www.indeed.com";

// Set up the context menus
chrome.contextMenus.create({
    "title": "Exclude This",
    "contexts": ["selection"],
    "onclick": function (e) {
        var buzzPostUrl = "";
        //alert(e.selectionText);
        if (e.selectionText) {
            var str = e.selectionText;
            chrome.tabs.executeScript(null, { file: "js/jquery.min.js" }, function () {
                chrome.tabs.executeScript(null, {
                    code: "var result = 10; var selectedTxt = '" + e.selectionText + "'; var divCollection = document.getElementsByTagName('span');" +
                    "for (var i=0; i<divCollection.length; i++) {" +
                    "if(divCollection[i].getAttribute('itemprop') == 'name') { console.log(divCollection[i]);" +
                    "if(divCollection[i] != null){ " +
                    "if(selectedTxt.trim() == divCollection[i].innerText.trim()){" +
                    " result = 20; break;}" +
                    "}} }  " +
                    "var checkforSpan = $('span.state-message'); if(checkforSpan != undefined && checkforSpan != null){ result = 30; console.log(result); }" +
                    "result"
                }, function (results) {

                    if (results == 20 || results == 30) {
                        get_manifest(function (manifest) {
                            var serviceUrl = manifest.update_url + "SearchService.svc/ChromeExtensionInsertExcludeText/" + franchiseId;

                            GetInsertSelectedText(serviceUrl, str, function (funreply1) {

                                if (funreply1 != undefined && funreply1 == "true") {

                                    var serviceUrlSearchTxt = manifest.update_url + "SearchService.svc/ChromeExtensionSearchTextByFranchise/" + franchiseId;

                                    AJAX_JSON_Req(serviceUrlSearchTxt, function (reply) {
                                        questions = [];
                                        reply.forEach(function (item) {
                                            questions.push("'" + item + "'");
                                        });

                                        chrome.tabs.getSelected(null, function (tab) {
                                            chrome.tabs.reload(tab.id);
                                        });
                                    });
                                }
                            });
                        });
                    }
                });
            });

        }

    }
});

chrome.contextMenus.create({
    "title": "Marketing sent",
    "contexts": ["selection"],
    "onclick": function (e) {
        var buzzPostUrl = "";
        //alert(e.selectionText);
        if (e.selectionText) {
            var str = e.selectionText;
            chrome.tabs.executeScript(null, { file: "js/jquery.min.js" }, function () {
                chrome.tabs.executeScript(null, {
                    code: "var result = 10; var selectedTxt = '" + e.selectionText + "'; var divCollection = document.getElementsByTagName('span');" +
                    "for (var i=0; i<divCollection.length; i++) {" +
                    "if(divCollection[i].getAttribute('itemprop') == 'name') { console.log(divCollection[i]);" +
                    "if(divCollection[i] != null){ " +
                    "if(selectedTxt.trim() == divCollection[i].innerText.trim()){" +
                    " result = 20; break;}" +
                    "}} }  " +
                    "var checkforSpan = $('span.state-message'); if(checkforSpan != undefined && checkforSpan != null){ result = 30; console.log(result); }" +
                    "result"
                }, function (results) {

                    if (results == 20 || results == 30) {
                        get_manifest(function (manifest) {
                            var serviceUrl = manifest.update_url + "SearchService.svc/ChromeExtensionInsertSelectedText/" + franchiseId + "/" + "1";

                            GetInsertSelectedText(serviceUrl, str, function (funreply1) {

                                if (funreply1 != undefined && funreply1 == "true") {

                                    var serviceUrlSearchTxt = manifest.update_url + "SearchService.svc/ChromeExtensionSearchTextByFranchise/" + franchiseId;

                                    AJAX_JSON_Req(serviceUrlSearchTxt, function (reply) {
                                        questions = [];
                                        reply.forEach(function (item) {
                                            questions.push("'" + item + "'");
                                        });

                                        chrome.tabs.getSelected(null, function (tab) {
                                            chrome.tabs.reload(tab.id);
                                        });
                                    });
                                }
                            });
                        });
                    }
                });
            });

        }

    }
});

function AJAX_JSON_Req(url, callback) {

    var AJAX_req = new XMLHttpRequest();
    AJAX_req.open("POST", url, true);
    AJAX_req.setRequestHeader("Content-type", "application/json");

    AJAX_req.onreadystatechange = function () {
        if (AJAX_req.readyState == 4 && AJAX_req.status == 200) {
            var jsonobj = JSON.parse(AJAX_req.responseText);
            callback(jsonobj);
        }
    };

    AJAX_req.send();
}

////Ajax function to call the service method and insert open link count to database
function InsertChromeExtension(url, params, callback) {
    var httpReq = new XMLHttpRequest();

    var str = params;
    str = str.replace("/", "slashsymbol");
    str = str.replace("&", "ampersandsymbol");
    str = str.replace("#", "hashsymbol");
    str = str.replace("?", "questionsymbol");

    httpReq.open("POST", url + "/" + str, true);

    httpReq.setRequestHeader("Content-type", "application/json");

    httpReq.onreadystatechange = function () {
        if (httpReq.readyState == 4 && httpReq.status == 200) {
            var jsonobj1 = httpReq.responseText;

            callback(jsonobj1);
        }
    };

    httpReq.send();
}

//function to get the manifest details.
function get_manifest(callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        callback(JSON.parse(xhr.responseText));
    };
    xhr.open('GET', './manifest.json', true);
    xhr.send(null);
}

//Ajax function to call the service method and insert selected text to database
function GetInsertSelectedText(url, params, callback) {
    var httpReq = new XMLHttpRequest();

    var str = params;
    str = str.replace("/", "slashsymbol");
    str = str.replace("&", "ampersandsymbol");
    str = str.replace("#", "hashsymbol");
    str = str.replace("?", "questionsymbol");
    str = str.replace(".", "dotsymbol");

    httpReq.open("POST", url + "/" + str, true);

    httpReq.setRequestHeader("Content-type", "application/json");

    httpReq.onreadystatechange = function () {
        if (httpReq.readyState == 4 && httpReq.status == 200) {
            var jsonobj1 = httpReq.responseText;
           
            callback(jsonobj1);
        }
    };

    httpReq.send();
}


//tab update event listerner......
//invoked any update in tabs
chrome.tabs.onUpdated.addListener(function (tabId, obj, tab) {

    //alert('https://' + jobSiteUrl + '/jobs/');
    if (obj && obj.status == "complete" && (tab.url.indexOf('http://' + jobSiteUrl + '/') == 0 || tab.url.indexOf('https://' + jobSiteUrl + '/') == 0)) {

        //alert(questions.length);
        if (questions.length == 0)
            chrome.browserAction.setIcon({ path: 'img/HighLightM19.png' });
        else
            chrome.browserAction.setIcon({ path: 'img/HighLight19.png' });


        chrome.tabs.executeScript(tabId, { file: "js/jquery.min.js" }, function () {

            if (chrome.runtime.lastError) {
                // alert(chrome.runtime.lastError.message);
            }

            setTimeout(function () {      
                chrome.tabs.executeScript(tabId, {
                    code: "var getText1 = ''; var newArray = [" + questions + "];  var isopened = " + isopenclicked + ";" +
                    "console.log('Word Count : ', newArray.length); console.log('Started...'); console.log('isopned :', isopened ); getText1 = '';" +
                    "var divCollection = document.getElementsByClassName('row result'); " +
                    "for (var i=0; i < divCollection.length; i++) {  var children = divCollection[i].getElementsByClassName('company'); var compn = divCollection[i].getElementsByClassName('jobtitle'); " +
                  //  "var compn = divCollection[i].getElementsByClassName('jobTitle'); " +
                    "if(compn['0'] != undefined){" +
                    "$.each(newArray,function(j,val){  " +
                    "var isMailSent = false; var textToCompare = val.trim(); var tooltipText =''; var textIndex = val.trim().indexOf('|O'); " +
                     "if( textIndex > 0){ isMailSent = true; textToCompare = val.trim().substr(0,textIndex); tooltipText = val.trim().substr(textIndex+2); }" +
                         "console.log(children[0].innerText.trim()); if(textToCompare != children['0'].innerText.trim()){ " +
                    " compn['0'].style.backgroundColor ='yellow';  }" +
                    "else if(textToCompare == children['0'].innerText.trim()){  " +
                    "if(isMailSent == true){  compn['0'].style.backgroundColor ='orange';  if(compn['0'].getElementsByTagName('a')[0] != undefined){ var textToAppend = compn['0'].getElementsByTagName('a')[0].innerHTML; compn['0'].getElementsByTagName('a')[0].innerHTML = textToAppend +'   ('+tooltipText +')';  }  return false;} else {" +
                    "compn['0'].style.backgroundColor='transparent';return false;} }});" +
                    "}}console.log('finished');"

                }, function (result1) {

                    if (tab.url != null) {

                        if ((tab.url.indexOf('http://' + jobSiteUrl + '/') == 0 || tab.url.indexOf('https://' + jobSiteUrl + '/') == 0) && isopenAllclicked == true && isopenclicked == false) {
                            chrome.extension.sendMessage({ command: "openAlllink", input: 'clicked' });
                        }
                    }
                });

            }, 1500);
        });
    }
});


//function to get the manifest details.
function OpenLink(sample) {

}

function deleteCookies(с) {
    for (var i = 0; i < с.length; i++) {
        try {
            var d = с[i];
            var u = ((d.secure) ? "https://" : "http://") + d.domain + d.path;
            chrome.cookies.remove({
                url: u,
                name: d.name,
                storeId: d.storeId
            });
        } catch (e) {
            alert("Error catched deleting cookie:\n" + e.description);
        }
    }
}

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        switch (request.command) {
            case "GetHTML":
                var deleter = function (cii) {
                    chrome.cookies.getAll({ domain: cookies[cii] }, function (f) {
                        deleteCookies(f);
                        if (cii < (cookies.length - 1)) {
                            cii++;
                            deleter(cii);
                        }
                    });
                }
                deleter(0);
                break;
            case "start":
                questions = [];
                request.input.forEach(function (item) {
                    questions.push("'" + item + "'");
                });

                chrome.tabs.getSelected(null, function (tab) {
                    chrome.tabs.reload(tab.id);
                });
                break;
            case "InsertSelected":
                alert("Inserted...");
                chrome.tabs.getSelected(null, function (tab) {
                    questions = [];
                    chrome.tabs.reload(tab.id);
                });

                break;
            case "StopOpenAllLink":
                var inputstring = request.input;
                if (inputstring == "clicked") {
                    isopenclicked = false;
                    isopenAllclicked = false;
                }
                break;
            case "openlink":
                isopenclicked = false;
                isopenAllclicked = false;
                var inputstring = request.input;
                if (inputstring == "clicked") {
                    isopenclicked = true;
                    chrome.tabs.query({ 'active': true }, function (tabs) {
                        chrome.tabs.getSelected(null, function (tab) {

                            chrome.tabs.executeScript(null, { file: "js/jquery.min.js" }, function () {

                                if (chrome.runtime.lastError) {
                                    // console.error(chrome.runtime.lastError.message);
                                }

                                chrome.tabs.executeScript(null, {
                                    code: "console.log('--------Only on this page ----------'); var getText = ''; var divCollection = document.getElementsByTagName('h2'); " +
                                    "for (var i=0; i<divCollection.length; i++) { " +
                                    "if(divCollection[i] != null && divCollection[i] != undefined && divCollection[i].getAttribute('class') == 'jobtitle'){  " +
                                    "if(divCollection[i].style.backgroundColor == 'yellow' || divCollection[i].style.backgroundColor == 'orange'){ console.log('2-'+divCollection[i]); var sparent1 =divCollection[i].getElementsByTagName('a'); " +
                                    "for (var c=0; c < sparent1.length; c++) {  if(sparent1[c].getAttribute('data-tn-element').indexOf('jobTitle') >= 0) {" +
                                    "  if(getText == ''){ getText = sparent1[c].href; }else{  getText=getText +'|'+ sparent1[c].href;}" +
                                    "} }  }}} getText"
                                }, function (result2) {

                                    var iLength2 = result2[0];
                                    var arr2 = [];
                                    arr2 = iLength2.split('|');
                                    j = 0;
                                    
                                    chrome.tabs.create({ 'url': arr2[j], 'selected': false }, function (tab) {
                                    });

                                    j++;
                                    var readyStateCheckInterval = setInterval(function () {
                                        if (j < arr2.length) {
                                            chrome.tabs.create({ "url": arr2[j], "active": false }, function (tab) {

                                            });
                                        }
                                        else {

                                            clearInterval(readyStateCheckInterval);
                                            arr2 = [];

                                            if (j > 1) {
                                                var openedLinkCount = j;

                                                get_manifest(function (manifest) {
                                                    var serviceUrl = manifest.update_url + "SearchService.svc/ChromeExtensionInsertExtensionDetails";

                                                    var strS = franchiseId + "|" + openedLinkCount;
                                                    InsertChromeExtension(serviceUrl, strS, function (funreply1) {

                                                        if (funreply1 != undefined && funreply1 == "true") {
                                                            
                                                            var serviceUrlSearchTxt = manifest.update_url + "SearchService.svc/ChromeExtensionSearchTextByFranchise/" + franchiseId;

                                                            AJAX_JSON_Req(serviceUrlSearchTxt, function (reply) {
                                                                questions = [];
                                                                reply.forEach(function (item) {
                                                                    questions.push("'" + item + "'");
                                                                });

                                                                chrome.tabs.getSelected(null, function (tab) {
                                                                    chrome.tabs.reload(tab.id);
                                                                });
                                                            });
                                                        }
                                                    });
                                                });
                                            }
                                        }

                                        j++;

                                    }, 6000);
                                });

                            });
                        });
                    });
                }
                break;
            case "openAlllink":
                isopenclicked = false;
                isopenAllclicked = false;

                var inputstring = request.input;
                if (inputstring == "clicked") {
                    isopenAllclicked = true;

                    chrome.tabs.query({ 'active': true }, function (tabs) {
                        chrome.tabs.getSelected(null, function (tab) {

                            chrome.tabs.executeScript(null, { file: "js/jquery.min.js" }, function () {

                                if (chrome.runtime.lastError) {
                                    // console.error(chrome.runtime.lastError.message);
                                }

                                chrome.tabs.executeScript(null, {
                                    code: "console.log('---------- all pages-------------'); var getText = ''; var divCollection = document.getElementsByTagName('h2');" +
                                    "for (var i=0; i<divCollection.length; i++) {" +
                                    "if(divCollection[i].getAttribute('class') == 'jobtitle') {" +
                                    "if(divCollection[i] != null){" +
                                    "if(divCollection[i].parentElement.style.backgroundColor == 'yellow' || divCollection[i].parentElement.style.backgroundColor == 'orange'){ console.log('3-'+divCollection[i]);" +
                                    "var sparent = divCollection[i].parentElement.parentElement.parentElement; var sparent1 = sparent.getElementsByTagName('span');  " +
                                    "for (var c=0; c < sparent1.length; c++) {  if(sparent1[c].getAttribute('itemprop') == 'title') { var parentdiv = sparent1[c].parentElement;" +
                                    " console.log(parentdiv.href);" +
                                    " if(getText == ''){ getText = parentdiv.href; }else{  getText=getText +'|'+ parentdiv.href;} " +
                                    "} } } } }} " +
                                    " var child = document.querySelector('.paging'); var childAA = document.getElementsByTagName('a'); " +
                                    "for (var d=0; d < childAA.length; d++) { if(childAA[d].getAttribute('class') == 'page-link') { " +
                                    "if(childAA[d] != null){  console.log(childAA[d].href); if(getText == ''){ getText = childAA[d].href; }else{  getText=getText +'|'+ childAA[d].href;} }" +
                                    "}} getText"                                  

                                }, function (result) {

                                    var iLength = result[0];

                                    if (iLength == null || iLength == '') {
                                        return;
                                    }

                                    var arr = [];
                                    arr = iLength.split('|');
                                    var i = 0;

                                    chrome.tabs.create({ 'url': arr[i], 'selected': false }, function (tab) {
                                    });

                                    i++;
                                    var readyStateCheckInterval1 = setInterval(function () {
                                        if (i < arr.length) {

                                            if (arr[i].indexOf('http://' + jobSiteUrl + '/') == 0 || arr[i].indexOf('https://' + jobSiteUrl + '/') == 0) {
                                                var newUrl = arr[i];

                                                chrome.tabs.query({ "active": true }, function (tabs) {
                                                    chrome.tabs.getSelected(null, function (tab) {
                                                        arr = [];
                                                        clearInterval(readyStateCheckInterval1);
                                                        chrome.tabs.update(tab.id, { "url": newUrl }, function (tab) {
                                                            chrome.tabs.reload(tab.id);
                                                        });


                                                    });

                                                });
                                            }
                                            else {
                                                chrome.tabs.create({ "url": arr[i], "active": false }, function (tab) {

                                                });
                                            }
                                        }
                                        i++;

                                    }, 6000);
                                });

                            });
                        });
                    });
                }
                break;
        }
        return true;
    }

);

