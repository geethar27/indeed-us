
//Ajax function to call the service method and read the search text from database
function AJAX_JSON_Req(url, callback) {

    var AJAX_req = new XMLHttpRequest();
    AJAX_req.open("POST", url, true);
    AJAX_req.setRequestHeader("Content-type", "application/json");

    AJAX_req.onreadystatechange = function () {
        if (AJAX_req.readyState == 4 && AJAX_req.status == 200) {
            var jsonobj = JSON.parse(AJAX_req.responseText);           
            callback(jsonobj);
        }
    };

    AJAX_req.send();
}

window.onload = function () {
    var btnYes = document.getElementById("btnYes");
    var btnNo = document.getElementById("btnNo");
    var urlLnk = document.getElementById("urlLnk");
    var urOpenlLnk = document.getElementById("urOpenlLnk");

    //No button click
    btnNo.onclick = function () {
        window.close();
    };

    //Yes button click
    btnYes.onclick = function () {
        chrome.extension.sendMessage({ command: "GetHTML" }, function (response) {
            console.log('cleraed cookies');
        });

        get_manifest(function (manifest) {            
            var serviceUrl = manifest.update_url + "SearchService.svc/ChromeExtensionSearchTextByFranchise/5";

            AJAX_JSON_Req(serviceUrl, function (reply) {
                chrome.extension.sendMessage({ command: "start", input: reply });
                window.close();
            });

        });
    };
    

    //Link button click
    urOpenlLnk.onclick = function () {

        chrome.extension.sendMessage({ command: "openlink", input: 'clicked' });
        window.close();
    };

    //Link button click
    urlLnk.onclick = function () {
        get_manifest(function (manifest) {
            var serviceUrl = manifest.update_url + "SearchService.svc/GetConnectToService";

            //var overrideUrl = "https://www.tender.net.au/admin/MonsterSearchTextDetails.aspx";
            var overrideUrl = "https://www.tender.net.au/admin/MarketingExcludedListing.aspx";

            GetConnectToService(serviceUrl, function (reply) {

                if (reply != undefined && reply != "") {
                    console.log(reply);
                    var str = reply.replace(/\"/g, "");

                    console.log(str);

                    var serviceAdUrl = manifest.update_url + "SearchService.svc/GetConnectAuthService";

                    GetConnectAuthService(serviceAdUrl, str, function (funreply) {
                        console.log(str);

                        if (funreply != undefined && funreply == "true") {
                            var overUrl = overrideUrl + "?param=" + str;
                            chrome.tabs.create({ url: overUrl });
                        }
                    });
                }
            });

        });
    };
}

//Ajax function to call the service method and connect to auth service from database
function GetConnectAuthService(url, params, callback) {
    var http = new XMLHttpRequest();   
    console.log(params);
    http.open("GET", url + "/" + params, true);

    http.setRequestHeader("Content-type", "application/json; charset=utf-8");  

    http.onreadystatechange = function () {
        if (http.readyState == 4 && http.status == 200) {
            var jsonobj = http.responseText;
            callback(jsonobj);
        }
    };

    http.send(params);   
}

//Ajax function to call the service method and connect to service from database
function GetConnectToService(url, callback) {

    var AJAX_req = new XMLHttpRequest();
    AJAX_req.open("POST", url, true);
    AJAX_req.setRequestHeader("Content-type", "application/json");

    AJAX_req.onreadystatechange = function () {
        if (AJAX_req.readyState == 4 && AJAX_req.status == 200) {          
            var jsonobj = AJAX_req.responseText;
            callback(jsonobj);
        }
    };

    AJAX_req.send();
}

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        switch (request.command) {
            case "bye": {
                alert('bye');
            } break;
            case "Hai": {
                if (request != undefined && request.input != "") {                   
                    var str = encodeURIComponent(request.input); //before you post the contents
                    //alert('hai');
                    get_manifest(function (manifest) {
                        var serviceUrl = manifest.update_url + "SearchService.svc/ChromeExtensionInsertSelectedText";

                        GetInsertSelectedText(serviceUrl, str, function (funreply1) {                        

                            if (funreply1 != undefined && funreply1 == "true") {

                                chrome.tabs.getSelected(null, function (tab) {
                                    alert(tab.id);
                                    chrome.tabs.reload(tab.id);
                                    window.close();
                                });                               
                            }
                        });
                    });
                }
                }
                break;
        }
        return true;
    }
);

//function to get the manifest details.
function get_manifest(callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        callback(JSON.parse(xhr.responseText));
    };
    xhr.open('GET', './manifest.json', true);
    xhr.send(null);
}

